require.config({
	paths: {
		primish: "components/primish"
	}
});

require(['primish/prime', 'primish/emitter', 'primish/options'], function(prime, emitter, options){

	var A = prime({
		options: {
			name: 'image',
			tag: 'span',
			src: '/'
		},
		implement: options,
		constructor: function(options){
			//console.debug(this.options);
			this.setOptions(options);
		},
		getOptions: function(){
			console.log(this.options);
		}
	});

	var B = prime({
		extend: A,
		options: {
			name: 'B'
		},
		some: function(){
			return;
		}
	});

	var C = prime({
		extend: B,
		options: {
			name: 'C'
		},
		some: function(){
			return;
		}
	});

	var c = new C({
		tag: 'div'
	});

	c.getOptions();

});