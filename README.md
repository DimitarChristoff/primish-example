demo for primish
================

```sh
[:~/projects] ± git clone https://DimitarChristoff@bitbucket.org/DimitarChristoff/primish-example.git
[:~/projects] ± cd primish-example
[:~/projects/primish-example] ± bower install
bower requirejs#~2.1.9          cached git://github.com/jrburke/requirejs.git#2.1.9
bower requirejs#~2.1.9        validate 2.1.9 against git://github.com/jrburke/requirejs.git#~2.1.9
bower primish#~0.2.0            cached git://github.com/DimitarChristoff/primish.git#0.2.0
bower primish#~0.2.0          validate 0.2.0 against git://github.com/DimitarChristoff/primish.git#~0.2.0
bower primish#~0.2.0           install primish#0.2.0
bower requirejs#~2.1.9         install requirejs#2.1.9

primish#0.2.0 dist/js/components/primish

requirejs#2.1.9 dist/js/components/requirejs
[:~/projects/primish-example] ± cd dist
[:~/projects/primish-example/dist]$ python -m SimpleHTTPServer
```

Then open `http://localhost:8000` and look at your console.

Recursive options mixing. See `dist/js/main.js`

```javascript
require.config({
	paths: {
		primish: "components/primish"
	}
});

require(['primish/prime', 'primish/emitter', 'primish/options'], function(prime, emitter, options){

	var A = prime({
		options: {
			name: 'image',
			tag: 'span',
			src: '/'
		},
		implement: options,
		constructor: function(options){
			//console.debug(this.options);
			this.setOptions(options);
		},
		getOptions: function(){
			console.log(this.options);
		}
	});

	var B = prime({
		extend: A,
		options: {
			name: 'B'
		},
		some: function(){
			return;
		}
	});

	var C = prime({
		extend: B,
		options: {
			name: 'C'
		},
		some: function(){
			return;
		}
	});

	var c = new C({
		tag: 'div'
	});

	c.getOptions();

});
```